/* ========================================================================== */
/*                                                                            */
/*   Node.cpp                                                                 */
/*   (c) 2018                                                                 */
/*                                                                            */
/*   Author:                                                                  */
/*   Arnoud van Leeuwen                                                       */
/*                                                                            */
/*   Description:                                                             */
/*   Defines the behaviour of a quadtree node                                 */
/* ========================================================================== */

#ifndef NODE_H
#define NODE_H


#ifndef QUADTREE_EXPORT
#define QUADTREE_API __declspec(dllimport)
#else
#define QUADTREE_API __declspec(dllexport)
#endif // !QUADTREE_EXPORT

#include <qrect.h>
#include <qpolygon.h>
#include <list>
#include <boost/signals2.hpp>
#include <boost/function.hpp>

namespace quadtree
{
    // Defining dummy classes that can be used in type definitions and the class definitions
    class  QUADTREE_API Node;
    class  QUADTREE_API Quadtree;

    /*-----------------------------------------------------------------------*/
    /*                                                                       */
    /*  Type definitions                                                     */
    /*                                                                       */
    /*-----------------------------------------------------------------------*/

    // Quadtree node list and iterators
    typedef QUADTREE_API std::list<const Node*> childlist;
    typedef QUADTREE_API childlist::iterator childiter;
    typedef QUADTREE_API childlist::const_iterator const_childiter;

    // Quadtree polygon list
    typedef QUADTREE_API std::list<QPolygonF> polylist;             
    typedef QUADTREE_API polylist::iterator poly_iter;
    typedef QUADTREE_API polylist::const_iterator const_poly_iter;

    // Callback function for call on rectangle creation
    typedef QUADTREE_API boost::function<void(const QRectF&)> qrectf_callback;

    /*-----------------------------------------------------------------------*/
    /*                                                                       */
    /*  Class definition                                                     */
    /*                                                                       */
    /*-----------------------------------------------------------------------*/
    class QUADTREE_API Node
    {
    public:
        Node(const polylist&, const QPolygonF&, const double& resolution, qrectf_callback);
        ~Node();
        double area() const;

    private:
        bool itc;
        QRectF rect;
        childlist children;
    };
    // End Node class definition

}
// End quadtree namespace

#endif // !NODE_H

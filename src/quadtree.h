#ifndef QUADTREE_H
#define QUADTREE_H

#include "node.h"

#include <list>
#include <qpolygon.h>
#include <boost/signals2.hpp>

namespace quadtree
{

    class QUADTREE_API Quadtree
    {
        childlist nodes;
        polylist list;
        float xmax, xmin, ymax, ymin, ar;
        float res; //Resolution
        boost::signals2::signal<void (const QRectF&)> nodeEvent; // Node creation event

    public:
        Quadtree(polylist&, float resolution);
        ~Quadtree();

        inline void insert(Node* n) { nodes.push_back(n); }
        inline const_poly_iter first_polygon() const { return list.begin(); }
        inline poly_iter first_polygon() { return list.begin(); }
        inline const_poly_iter last_polygon() const { return list.end(); }
        inline poly_iter last_polygon() { return list.end(); }
        inline float width() const { return xmax - xmin; }
        inline float height() const { return ymax - ymin; }
        inline float resolution() const { return res; }
        inline void setResolution(float &r) { res = r; }
        double area();
        void checkIntersect(const QPolygonF&);
        void devide(); // Devides the model area in as many squares as possible
        void registerNodeCallback(boost::function<void(const QRectF&)>);

    protected:
        void newnode_callback(const QRectF&);
        void hSplit(const QRectF&);
        void vSplit(const QRectF&);
    };
}
#endif // !QUADTREE_H

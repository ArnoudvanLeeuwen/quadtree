#include "quadtree.h"
#include <iostream>
#include <iomanip>

using namespace std;
using namespace quadtree;

Quadtree::Quadtree(polylist &l, float r)
    : list(l), xmax(FLT_MIN), xmin(FLT_MAX), ymax(FLT_MIN), ymin(FLT_MAX), ar(0.0f), nodes(), res(r)
{
    // Fint the polygon min max values
    for (auto it(list.begin()); it != list.end(); ++it) {
        for each(QPointF p in *it)
        {
            if (p.x() > xmax) xmax = p.x();
            else if (p.x() < xmin) xmin = p.x();
            if (p.y() > ymax) ymax = p.y();
            else if (p.y() < ymin) ymin = p.x();
        }
    }

    //Calculate aspect ratio
    ar = width() / height();
}

Quadtree::~Quadtree()
{
    nodeEvent.disconnect_all_slots();
}

double Quadtree::area()
{
    double a(0);
    for (const_childiter it(nodes.begin()); it != nodes.end(); it++) {
        a += (*it)->area();
    }
    return a;
}

void quadtree::Quadtree::checkIntersect(const QPolygonF &p)
{
    for (auto it(first_polygon()); it != last_polygon(); ++it) {
        if (it->intersects(p)) {
            Node *n = new Node(list, p, res, boost::bind(&Quadtree::newnode_callback, this, _1));
            nodes.push_back(n);
            return;
        }
    }
}

void quadtree::Quadtree::devide()
{
    QRectF rect(xmin, ymin, width(), height());

    if (ar > 1) { // Width greater than height
        hSplit(rect);
    }
    else if (ar < 1) { //Height is greater than the width
        vSplit(rect);
    }
    else { // Perfect square, split in 4 quadrants
        QPolygonF p1, p2, p3, p4;

        p1 << QPointF(xmin, ymin)
            << QPointF(xmin + width() / 2, ymin)
            << QPointF(xmin + width() / 2, ymin + height() / 2)
            << QPointF(xmin, ymin + height() / 2);

        p2 << QPointF(xmin + width() / 2, ymin)
            << QPointF(xmax, ymin)
            << QPointF(xmax, ymin + height() / 2)
            << QPointF(xmin + width() / 2, ymin + height() / 2);

        p3 << QPointF(xmin, ymin + height() / 2)
            << QPointF(xmin + width() / 2, ymin + height() / 2)
            << QPointF(xmin + width() / 2, ymax)
            << QPointF(xmin, ymax);

        p4 << QPoint(xmin + width() / 2, ymin + height() / 2)
            << QPointF(xmax, ymin + height() / 2)
            << QPoint(xmax, ymax)
            << QPointF(xmax - width() / 2, ymax);

        checkIntersect(p1);
        checkIntersect(p2);
        checkIntersect(p3);
        checkIntersect(p4);
    }
}

void quadtree::Quadtree::registerNodeCallback(boost::function<void(const QRectF&)> f)
{
    nodeEvent.connect(f);
}

void quadtree::Quadtree::newnode_callback(const QRectF &r)
{
    nodeEvent(r);
}

void quadtree::Quadtree::hSplit(const QRectF & r)
{
    float pos(0);
    for (int i = 0; i < (ar + 1); ++i) {
        QPolygonF p1, p2;

        p1 << QPointF(xmin + i*height() / 2, ymin)
            << QPointF(xmin + (i + 1)*height() / 2, ymin)
            << QPointF(xmin + (i + 1)*height() / 2, ymin + height() / 2)
            << QPointF(xmin + i*height() / 2, ymin + height() / 2);

        p2 << QPointF(xmin + i*height() / 2, ymin + (height() / 2))
            << QPointF(xmin + (i + 1)*height() / 2, ymin + (height() / 2))
            << QPointF(xmin + (i + 1)*height() / 2, ymin + height())
            << QPointF(xmin + i*height() / 2, ymin + height());

        checkIntersect(p1);
        checkIntersect(p2);

        pos = i*height() / 2;
    }
}

void quadtree::Quadtree::vSplit(const QRectF & r)
{
    float pos;
    for (int i(0); i < (1.0 / ar) + 1; ++i) {
        QPolygonF p1, p2;
        p1 << QPointF(xmin, ymin + i*width() / 2)
            << QPointF(xmin + width() / 2, ymin + i*width() / 2)
            << QPointF(xmin + width() / 2, ymin + (i + 1)*width() / 2)
            << QPointF(xmin, ymin + (i + 1)*width() / 2);
        p2 << QPointF(xmin + width() / 2, ymin + i*width() / 2)
            << QPointF(xmin + width(), ymin + i*width() / 2)
            << QPointF(xmin + width(), ymin + (i + 1)*width() / 2)
            << QPointF(xmin + width() / 2, ymin + (i + 1)*width() / 2);

        checkIntersect(p1);
        checkIntersect(p2);

        pos = i*width() / 2;
    }
}

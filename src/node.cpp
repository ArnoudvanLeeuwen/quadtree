#include "node.h"
#include <exception>

using namespace quadtree;

bool embodies(const QPolygonF& mdl_poly, const QPolygonF& quad_poly)
{
    // Iterate the points in the quad polygon to check if all lie within the model polygon
    for (auto it(quad_poly.begin()); it != quad_poly.end(); ++it) {
        if (!mdl_poly.containsPoint((*it), Qt::FillRule::OddEvenFill)) {
            return false;
        }
    }
    return true;
}

quadtree::Node::Node(const polylist &list, const QPolygonF &poly, const double& r, qrectf_callback cb)
    : itc(false), rect(poly.boundingRect()), children()
{
    // Callback
    cb(rect);

    // Only continue when the minimum resolution has not ben reached
    if (rect.width() < r && rect.height() < r) {
        itc = true;
        return;
    }

    // Check for polygon intersection by the specified rectangle
    // If no intersection occurs, create no children
    for (auto it(list.begin()); it != list.end(); ++it) {
        if (it->intersects(poly)) {
            itc = true;
            if (embodies(*it, poly)) return;
        }
    }
    if (!itc) return;

    // Devide polygon into 4 polygons
    QPolygonF p_a, p_b, p_c, p_d;
    p_a << rect.topLeft()
        << QPointF(rect.left() + rect.width() / 2, rect.top())
        << QPointF(rect.left() + rect.width() / 2, rect.top() + rect.height() / 2)
        << QPointF(rect.left(), rect.top() + rect.height() / 2);

    p_b << QPointF(rect.left() + rect.width() / 2, rect.top())
        << rect.topRight()
        << QPointF(rect.right(), rect.top() + rect.height() / 2)
        << QPointF(rect.left() + rect.width() / 2, rect.top() + rect.height() / 2);

    p_c << QPointF(rect.left(), rect.top() + rect.height() / 2)
        << QPointF(rect.left() + rect.width() / 2, rect.top() + rect.height() / 2)
        << QPointF(rect.left() + rect.width() / 2, rect.bottom())
        << rect.bottomLeft();

    p_d << QPointF(rect.left() + rect.width() / 2, rect.top() + rect.height() / 2)
        << QPointF(rect.right(), rect.top() + rect.height() / 2)
        << rect.bottomRight()
        << QPointF(rect.left() + rect.width() / 2, rect.bottom());

    // For each polygon, create a new child
    children.push_back(new Node(list, p_a, r, cb));
    children.push_back(new Node(list, p_b, r, cb));
    children.push_back(new Node(list, p_c, r, cb));
    children.push_back(new Node(list, p_d, r, cb));

}

Node::~Node()
{
}

double Node::area() const
{

    /* When the node has no children and the polygon intersects with any
     * polygon in the model polygon list, return the surface of the* rectangle.
     */
    if (children.size() == 0 && itc) {
        return rect.width()*rect.height();
    }

    /* When the node does have children, iterate all of them and add their
     * surfaces to an area boolean.
     * This happens when the Node has not intersection with the models polygons
     * as well (it will have no children in that case, thus returning 0.
     */
    double a(0);
    for (auto it(children.begin()); it != children.end(); ++it) {
        a += (*it)->area();
    }
    return a;
}
